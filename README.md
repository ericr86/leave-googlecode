
This is a **bunch of python** to help migrating away from google-code into other issue tracking services.
So far this only supports Bitbucket - but other services with suitable APIs could be easilly added.

- bitbucket.py	- Get/Create issues and comments
- googlecode.py	- Get issue information from Google Code
- common.py	- Some common methods and classes
- google2btb.py	- Example code to migrate issues from Google Code to Bitbucket

As it is this is only suitable to migrate issues from Google Code into Bitbucket - see google2btb.py


