from __future__ import print_function, unicode_literals

from gdata.projecthosting.client import ProjectHostingClient, Query
from common import *


def google_get_comments(client, project_name, issue_id):
    comments = client.get_comments(project_name, issue_id)
    return comments.entry

def google_get_issue( project_name, issue_id, client=ProjectHostingClient()):

    query = Query(issue_id=issue_id)
    feed = client.get_issues(project_name, query=query)

    issue = feed.entry[0]

    issuetype = ''
    priority = ''
    for label in issue.label:
        try:
            name, val = label.text.split('-', 1)
        except ValueError:
            continue

        if name == 'Type':
            issuetype = val
        elif name == 'Priority':
            priority = val.lower()

    commentlist = []
    comments = google_get_comments(client, project_name, issue_id)
    for comment in comments:
        if comment.content.text:
            commentlist.append( IssueComment( comment.author[0].name.text, comment.content.text) )

    content = striphtml(issue.content.text )
    return Issue( issue.title.text,
                issue.author[0].name.text,
                status=issue.status.text,
                issuetype=issuetype,
                priority = priority,
                content=content, comments=commentlist,
                imported_from='Google Code(%s#%s)' % (project_name,issue_id))

if __name__ == '__main__':

    import sys

    if len(sys.argv) != 3:
        print( 'Usage: %s <google project name> <issue id>' % sys.argv[0] )
        sys.exit(-1)

    issue = google_get_issue(sys.argv[1], sys.argv[2])
    print( issue.__unicode__() )


