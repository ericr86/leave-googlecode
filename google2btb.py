from __future__ import print_function, unicode_literals
from googlecode import google_get_issue
from bitbucket import Api

if __name__ == '__main__':
    import sys

    #
    # This would get issue #4 from code.google.com/vim-qt and
    # copy it to bitbucket.org/equalsraf/vim-qt
    #

    gc_project = 'vim-qt'
    gc_issue = '4'

    username = 'BITBUCKET_USERNAME'
    password = 'BITBUCKET_PASSWORD'
    bitbucket = Api('equalsraf', 'vim-qt', username, password)

    issue = google_get_issue(gc_project, gc_issue)
    issue_id = bitbucket.create_issue( issue )

    for comment in issue.comments():
        bitbucket.create_issue_comment( issue_id, comment )


