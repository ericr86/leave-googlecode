from __future__ import print_function, unicode_literals
import urllib2, base64, json, urllib
from common import *

SEARCH_ISSUES_URL = 'https://api.bitbucket.org/1.0/repositories/%s/%s/issues/'
GET_ISSUE_URL = 'https://api.bitbucket.org/1.0/repositories/%s/%s/issues/%d/'
NEW_ISSUE_URL = 'https://api.bitbucket.org/1.0/repositories/%s/%s/issues/'
NEW_ISSUE_COMMENT_URL = 'https://api.bitbucket.org/1.0/repositories/%s/%s/issues/%d/comments/'
DELETE_ISSUE_URL = GET_ISSUE_URL # The URL is the same but the HTTP Method is DELETE
GET_EVENTS_REPOSITORY ='https://api.bitbucket.org/1.0/repositories/%s/%s/events/'


ISSUE_BUG = 'bug'
ISSUE_ENHANCEMENT = 'enhancement'
ISSUE_PROPOSAL = 'proposal'
ISSUE_TASK = 'task'

ISSUE_KIND = { ISSUE_BUG : ISSUE_BUG,
                ISSUE_ENHANCEMENT : ISSUE_ENHANCEMENT,
                ISSUE_PROPOSAL : ISSUE_PROPOSAL,
                ISSUE_TASK : ISSUE_TASK,
                'defect' : ISSUE_BUG,
                'review' : ISSUE_TASK,
                'other' : ISSUE_TASK}

ISSUE_NEW = 'new'
ISSUE_OPEN = 'open'
ISSUE_RESOLVED = 'resolved'
ISSUE_ON_HOLD = 'on hold'
ISSUE_INVALID = 'invalid'
ISSUE_DUPLICATE = 'duplicate'
ISSUE_WONTFIX = 'wontfix'

ISSUE_STATUS = {
        ISSUE_NEW : ISSUE_NEW,
        ISSUE_OPEN : ISSUE_OPEN,
        'accepted' : ISSUE_OPEN,
        'started' : ISSUE_OPEN,
        ISSUE_RESOLVED : ISSUE_RESOLVED,
        'fixed' : ISSUE_RESOLVED,
        'closed' : ISSUE_RESOLVED,
        ISSUE_ON_HOLD : ISSUE_ON_HOLD,
        ISSUE_INVALID : ISSUE_INVALID,
        ISSUE_DUPLICATE : ISSUE_DUPLICATE,
        ISSUE_WONTFIX : ISSUE_WONTFIX,
        }

ISSUE_TRIVIAL = 'trivial'
ISSUE_MINOR = 'minor'
ISSUE_MAJOR = 'major'
ISSUE_CRITICAL = 'critical'
ISSUE_BLOCKER = 'blocker'

ISSUE_PRIORITY = {
        ISSUE_TRIVIAL : ISSUE_TRIVIAL,
        'low' : ISSUE_TRIVIAL,
        'medium' : ISSUE_MAJOR,
        'high' : ISSUE_MAJOR,
        'critical' : ISSUE_CRITICAL,
        }

def issue2dict(issue):

    d = {}
    author_line = '\n\n^^//[Import-Original-Author: %s]//^^\n\n' % issue.author()
    import_line = '^^//[This issue was automatically imported from %s, some data may have been lost ]//^^\n\n' % issue.imported_from()
    content = import_line + author_line + issue.content().replace('\n','\n\n')

    d['title'] = issue.title().encode('utf-8')
    d['content'] = content.encode('utf-8')

    d['status'] = ISSUE_STATUS.get(issue.status().lower(), ISSUE_NEW).encode('utf-8')
    d['kind'] = ISSUE_KIND.get(issue.kind().lower(), ISSUE_BUG).encode('utf-8')
    d['priority'] = ISSUE_PRIORITY.get(issue.priority(), ISSUE_MINOR).encode('utf-8')

    return d


def comment2dict(comment):

    content = comment.content()
    content = '\n\n^^//[Import-Original-Author: %s]//^^\n\n' % comment.author() + content
    return {'content' : content.encode('utf-8')}


class Api:

    def __init__(self, owner, repo, login='', password=''):
        self.owner = owner
        self.repo = repo
        self.login = login
        self.password = password

    def search_issues(self, **kwargs):
        url = SEARCH_ISSUES_URL % (self.owner, self.repo) + '?' + urllib.urlencode(kwargs)

        f = urllib2.urlopen(urllib2_request(url, self.login, self.password))
        obj = json.loads( f.read() )
        return obj

    def get_issue(self, issue_id):
        """
        Get issue with the given id
        """
        url = GET_ISSUE_URL % (self.owner, self.repo, int(issue_id) )
        f = urllib2.urlopen(urllib2_request(url, self.login, self.password))
        obj = json.loads( f.read() )

        if obj.has_key('reported_by'):
            author = obj['reported_by']['username']
        else:
            author = 'anonymous'

        return Issue(obj['title'], author, issuetype=obj['metadata']['kind'], content=obj['content'], status=obj['status'], priority=obj['priority'])


    def create_issue_comment(self, issue_id, comment):

        url = NEW_ISSUE_COMMENT_URL % (self.owner, self.repo, issue_id)
        data = urllib.urlencode( comment2dict(comment) )
        f = urllib2.urlopen(urllib2_request(url, self.login, self.password), data)

    def create_issue(self, issue):
        """
        Creates a new issue in Bitbucket

        Returns the id of the new issue
        """
        url = NEW_ISSUE_URL % (self.owner, self.repo)

        data = urllib.urlencode( issue2dict(issue) )

        f = urllib2.urlopen(urllib2_request(url, self.login, self.password), data)
        response = json.loads(f.read())

        return response['local_id']

    def delete_issue(self, issue_id):
        """
        Delete issue
        """
        url = DELETE_ISSUE_URL % (self.owner, self.repo, issue_id)

        f = urllib2.urlopen(urllib2_request(url, self.login, self.password,
            method='DELETE') )

    def get_events(self, **kwargs):
        url = GET_EVENTS_REPOSITORY % (self.owner, self.repo) + '?' + urllib.urlencode(kwargs)
        f = urllib2.urlopen(urllib2_request(url, self.login, self.password) )
        response = json.loads(f.read())
        return response



