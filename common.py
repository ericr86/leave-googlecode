from __future__ import print_function, unicode_literals
from HTMLParser import HTMLParser
import urllib2, base64


class MLStripper(HTMLParser):
    def __init__(self):
        self.reset()
        self.fed = []
    def handle_data(self, d):
        self.fed.append(d)
    def get_data(self):
        return ''.join(self.fed)

def striphtml(html):
    """
    Turn html text into plaint text
    """
    s = MLStripper()
    s.feed(html)
    return s.get_data()

def urllib2_request(url, username='', password='', method=''):
    """
    Create an urllib2 Request object

    If username and password are set the request will use HTTP Basic Auth
    """

    req = urllib2.Request(url)

    if method:
        req.get_method = lambda: method

    if username and password:
        auth = base64.encodestring('%s:%s' % (username, password)).replace('\n', '')
        req.add_header("Authorization", "Basic %s" % auth)

    return req


class IssueComment:

    def __init__(self, author, content):
        self.__author = author
        self.__content = content

    def author(self):
        return self.__author

    def content(self):
        return self.__content

    def __unicode__(self):
        return """# %s
%s""" % (self.author(), self.content())

class Issue:

    def __init__(self, title, author, status='', issuetype='', priority='',
            content='', comments=None, imported_from=''):
        self.__title = title
        self.__author = author
        self.__status = status
        self.__type = issuetype
        self.__priority = priority
        self.__content = content
        if comments:
            self.__comments = comments
        else:
            self.__comments = []
        self.__imported = imported_from

    def title(self):
        return self.__title

    def author(self):
        return self.__author

    def status(self):
        return self.__status

    def issuetype(self):
        return self.__type
    kind = issuetype

    def priority(self):
        return self.__priority

    def content(self):
        return self.__content

    def comments(self):
        return self.__comments

    def __unicode__(self):

        comments_str = '\n'.join( [x.__unicode__() for x in self.comments()])

        return """Author: %s
Title: %s
Status: %s
Kind: %s
Priority: %s
Content: 
%s

%s""" % (self.author(), self.title(), self.status(), self.kind(),
                        self.priority(), self.content(), comments_str)


    def imported_from(self):
        return self.__imported

